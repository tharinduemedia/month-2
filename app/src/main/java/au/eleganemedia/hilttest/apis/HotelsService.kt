package au.eleganemedia.hilttest.apis

import au.eleganemedia.hilttest.models.Hotel
import au.eleganemedia.hilttest.utils.Constants.HOTELS_END_POINT
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers

interface HotelsService {
    @Headers("Content-Type:application/json")
    @GET(HOTELS_END_POINT)
    suspend fun getAllHotels(): Response<Hotel>
}