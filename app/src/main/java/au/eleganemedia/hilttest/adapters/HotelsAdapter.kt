package au.eleganemedia.hilttest.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import au.eleganemedia.hilttest.databinding.RowHotelBinding
import au.eleganemedia.hilttest.models.Data
import coil.load
import com.squareup.picasso.Picasso

class HotelsAdapter : RecyclerView.Adapter<HotelsAdapter.HotelsViewHolder>() {
    inner class HotelsViewHolder(
        val binding: RowHotelBinding
    ) :
        RecyclerView.ViewHolder(binding.root)

    private val diffCallback = object : DiffUtil.ItemCallback<Data>() {
        override fun areItemsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem == newItem
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    fun submitList(list: List<Data>) = differ.submitList(list)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HotelsViewHolder {
        return HotelsViewHolder(
            RowHotelBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: HotelsViewHolder, position: Int) {
        val currentHotel = differ.currentList[position]
        holder.binding.apply {
            textViewHotel.text = currentHotel.title
            textViewHotelAddress.text = currentHotel.address
            val imageLink = currentHotel.image.large
            Picasso.get().load(imageLink).into(imageViewHotel);
        }
    }

    override fun getItemCount() = differ.currentList.size
}
