package au.eleganemedia.hilttest.repositories

import au.eleganemedia.hilttest.apis.HotelsService
import javax.inject.Inject

class HotelsRepository
@Inject constructor(private val api: HotelsService) {
    suspend fun getAllHotels() = api.getAllHotels()
}
